# Food 'O' Quiz

This is a Food Quiz App made using the Flutter Framework.

It suggests the user what to eat depending on the options chosen by them. 
It also gives the user an option to view the recipe of the food item suggested by the app.

![menu1](/uploads/d099e9966b611c21c5b6347b305d7a15/menu1.mp4)

![menu2](/uploads/9bbd0b3b62b20217097f9a5fbbf7a9e3/menu2.mp4)




